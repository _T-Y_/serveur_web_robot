var http = require('http').createServer(handler); //require http server, and create server with function handler()
var fs = require('fs'); //require filesystem module
var io = require('socket.io')(http) //require socket.io module and pass the http object (server)

var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var ledRouge = new Gpio(23, 'out'); //use GPIO pin 4 as output

http.listen(8080); //listen to port 8080

function handler (req, res) { //create server
  fs.readFile(__dirname + '/Public/index.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    } 
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    res.write(data); //write data from index.html
    return res.end();
  });
}


io.sockets.on('connection', function (socket) {   // WebSocket Connection
  var lightvalue = 0;
                                                  // static variable for current status
  pushButton.watch(function (err, value) {        // Watch for hardware interrupts on pushButton
    if (err) { //if an error
      console.error('There was an error', err);   // output error message to console
      return;
    }
    lightvalue = value;
    socket.emit('light', lightvalue);             // send button status to client
  });

  socket.on('rouge', function(data) {             // get light switch status from client
    lightvalue = data;
    if (lightvalue != LED.readSync()) {           // only change LED if status has changed
      LED.writeSync(lightv

  });
});
